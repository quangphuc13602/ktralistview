package com.example.ktralistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EmployeeAdapter employeeAdapter;
    private List<Employee> list;

    public MainActivity(){
        list = new ArrayList<>();

        list.add(new Employee(1, "Đừng chờ anh nữa",R.drawable.music, "Tăng Phúc"));
        list.add(new Employee(1, "Dấu mưa",R.drawable.music, "Trung Quân"));
        list.add(new Employee(1, "Sau cơn mê",R.drawable.music, "Lệ QUyên"));
        list.add(new Employee(1, "IF",R.drawable.music, "Vũ Cát Tường"));
        list.add(new Employee(1, "Trời giấu trời mang đi",R.drawable.music, "Amee"));
        list.add(new Employee(1, "Em là lí do anh say",R.drawable.music, "Bray - Viruss"));
        employeeAdapter = new EmployeeAdapter(this, list);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lv = findViewById(R.id.lvEmployees);
        lv.setAdapter(employeeAdapter);
    }
}