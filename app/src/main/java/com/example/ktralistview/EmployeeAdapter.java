package com.example.ktralistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class EmployeeAdapter extends BaseAdapter {
    private Context context;

    public EmployeeAdapter(Context context, List<Employee> list) {
        this.context = context;
        this.list = list;
    }

    private List<Employee> list;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        if (position < 0)
            return null;

        return list.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null){
            view = LayoutInflater.from(context).inflate(R.layout.layout_employee_item, null);
        }
        TextView tvname = view.findViewById(R.id.lvName);
        TextView tvbref = view.findViewById(R.id.lvbrief);
        ImageView ivImage = view.findViewById(R.id.imageView);

        Employee emp = list.get(i);
        tvname.setText(emp.getName());
        tvbref.setText(emp.getBrief());
        ivImage.setImageResource(emp.getImageid());
        return view;
    }
}
