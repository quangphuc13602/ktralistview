package com.example.ktralistview;

public class Employee {
    private int employeeid;
    private String name;
    private  int imageid;
    private String brief;


    public Employee() {

    }

    public Employee(int employeeid, String name, int imageid, String brief) {
        this.employeeid = employeeid;
        this.name = name;
        this.imageid = imageid;
        this.brief = brief;
    }

    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }
}
